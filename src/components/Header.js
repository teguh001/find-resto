import React, {useState} from 'react'
import { connect } from 'react-redux'
import { searchRestaurants } from '../redux/actions'
import { Autocomplete } from '@react-google-maps/api';
import { Input } from 'antd';

const Header = ({ dispatch, setCoordinates }) => {
  // const keywordInput = React.createRef()
  const [autocomplete, setAutocomplete] = useState(null);
  const onLoad = (autoC) => setAutocomplete(autoC);
  const onPlaceChanged = () => {
    console.log('see auto', autocomplete.getPlace().name)
    const keyword = autocomplete.getPlace().name
    dispatch(searchRestaurants({ keyword }))
  }

  // function search() {
  //   const keyword = keywordInput.current.value
  //   dispatch(searchRestaurants({ keyword }))
  //   console.log('see keyword', keyword)
  // }

  return (
    <React.Fragment>
      <nav className="navbar navbar-dark bg-dark">
        <a className="navbar-brand" href="/">Restaurants Finder</a>
      </nav>
      {/* <Autocomplete onLoad={onLoad} onPlaceChanged={onPlaceChanged}> */}
      <Autocomplete onLoad={onLoad} onPlaceChanged={onPlaceChanged}> 
        <div className="form-inline my-2">
          <Input className="form-control" placeholder="Search…"  />
        </div>
      </Autocomplete>
      {/* <form className="form-inline my-2" onSubmit={e => e.preventDefault()}>
        <input className="form-control" ref={keywordInput} placeholder="keyword" />
        <button className="btn btn-secondary ml-2" onClick={e => search(e)}>Search</button>
      </form> */}
    </React.Fragment>
  )
}

export default connect()(Header)