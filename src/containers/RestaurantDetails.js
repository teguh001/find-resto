import React from 'react'
import { connect } from 'react-redux'
import { showRestaurantDetails } from '../redux/actions'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Box, Typography, Button, Card, CardMedia, CardContent, CardActions, Chip } from '@material-ui/core';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIcon from '@material-ui/icons/Phone';
import Rating from '@material-ui/lab/Rating';
import useStyles from './styles.js';

const RestaurantDetails = ({ restaurant, restaurant: { id, name, thumb }, dispatch }) => {
  if (!restaurant) {
    return null
  }
  const classes = useStyles();

  return (
    <div className="card restaurant-details">
      <div className="card-header">
        {/* {name} */}
        <button className="modal-close" onClick={() => dispatch(showRestaurantDetails())}>&times;</button>
      </div>
      <div className="card-body">
        {/* <div className="card-text">
          <img src={thumb} className="mr-3" alt={name} style={{ width: 64 }} />
          <pre>{JSON.stringify(restaurant.location, null, 1)}</pre>
        </div> */}

        <Card elevation={0} >
          <CardMedia
            style={{ height: 350 }}
            image={thumb}
            title={name}
          />
          <CardContent style={{ paddingLeft: 0, paddingRight: 0 }}>
            <Typography gutterBottom variant="h5">{name}</Typography>
            <Box display="flex" justifyContent="space-between" my={2}>
              <Rating name="read-only" value={Number(restaurant.user_rating.aggregate_rating)} readOnly />
              <Typography component="legend">{restaurant.all_reviews_count} review{restaurant.all_reviews_count > 1 && 's'}</Typography>
            </Box>
            <Box display="flex" justifyContent="space-between">
              <Typography component="legend">Price</Typography>
              <Typography gutterBottom variant="subtitle1">
                {restaurant.price_range}
              </Typography>
            </Box>
            {restaurant.location.address && (
              <Typography gutterBottom variant="body2" color="textSecondary" className={classes.subtitle}>
                <LocationOnIcon />{restaurant.location.address}
              </Typography>
            )}
            {restaurant.phone_numbers && (
              <Typography variant="body2" color="textSecondary" className={classes.spacing}>
                <PhoneIcon /> {restaurant.phone_numbers}
              </Typography>
            )}
            <Box style={{ paddingTop: '30px' }}>
              {restaurant.cuisines.split(',').map(name => (
                <Chip key={name} size="small" label={name} className={classes.chip} />
              ))}
            </Box>
          </CardContent>
          <CardActions>
            <Button size="small" color="primary" onClick={() => window.open(restaurant.url, '_blank')}>
              Website
            </Button>
          </CardActions>
        </Card>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  const { application } = state

  return { application }
}

export default connect(mapStateToProps)(RestaurantDetails)